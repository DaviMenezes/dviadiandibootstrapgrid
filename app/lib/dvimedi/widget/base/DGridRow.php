<?php

namespace DviMedi\Widget\Base;

/**
 * Description of DGridRow
 *
 * @author DAVI MENEZES
 */
use Adianti\Widget\Base\TElement;
use \Adianti\Widget\Form\TLabel;
use DviMedi\Widget\Base\DGridColumn;

/**
 * Row to boostrap grid
 *
 * @version    Adianti 4.0
 * @package    grid bootstrap
 * @subpackage base
 * @author     Davi Menezes
 * @copyright  Copyright (c) 2017. (davimenezes.dvi@gmail.com)
 * @link https://bitbucket.org/DaviMenezes/dviadiandibootstrapgrid
 */
class DGridRow extends TElement {

    private $chilStyle;
    private $defaultColClass;
    private $defaultColStyle;
    private $columns = array();
    private $maxColumns = 12;

    public function __construct($rowStyle, $defaultColClass = NULL, $colStyle = NULL)
    {
        //cria uma linha
        parent::__construct('div');

        $this->class = 'row';
        $this->{'style'} = 'clear:both; margin:5px 0; ';
        $this->style .= $rowStyle . '; margin-top:5px';

        $this->defaultColClass = $defaultColClass ?? 'col-md-6';
        $this->defaultColStyle = $colStyle;
        $this->chilStyle = '';
    }

    public function addCol($child, $colClass, $colStyle = NULL)
    {
        $this->columns[] = ['child' => $child, 'class' => $colClass, 'style' => $colStyle];
    }

    public function addCols(array $childs)
    {
        $defaultClass = 'col-md-' . ($this->maxColumns / count($childs));

        foreach ( $childs as $child ) {
            $this->columns[] = ['child' => $child, 'class' => $defaultClass, 'style' => $this->defaultColStyle];
        }

        $this->prepareColumns();
    }

    public function addLabel($title)
    {
        $this->addCol(new TLabel($title), 'col-md-2');
    }

    //must to be public
    public function prepareColumns(): array
    {
        foreach ( $this->columns as $column ) {
            $col = new DGridColumn($column['child'], $column['class'], $column['style']);
            parent::add($col);
        }
        return $this->columns;
    }

}