<?php
namespace DviMedi\Widget\Base;

/**
 * Manipulação de grids bootstraps
 *
 * @version    Adianti 4.0
 * @package    grid bootstrap
 * @subpackage base
 * @author     Davi Menezes
 * @copyright  Copyright (c) 2017. (davimenezes.dvi@gmail.com)
 * @link https://bitbucket.org/DaviMenezes/dviadiandibootstrapgrid
 */
use DviMedi\Widget\Base\DGridRow;
use Adianti\Widget\Base\TElement;
class DGridBootstrap
{
    private $grid;
    private $defaultColClass;
    private $defaultColStyle;
    private $rows = array();

    public function __construct($defaultColClass = NULL, $colStyle = NULL)
    {
        $this->grid = new TElement('div');

        $this->defaultColClass = $defaultColClass;
        $this->defaultColStyle = $colStyle;

    }

    public function addRow(string $rowStyle = NULL): DGridRow
    {

        $row = new DGridRow($rowStyle, $this->defaultColClass, $this->defaultColStyle);
        $this->grid->add($row);

        $this->rows[] = $row;

        return $row;
    }

    public function show(){
        foreach ($this->rows as $row) {
            $row->prepareColumns();
        }
        $this->grid->show();
    }
}