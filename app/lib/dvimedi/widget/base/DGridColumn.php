<?php

namespace DviMedi\Widget\Base;

/**
 * Description of DGridColumn
 *
 * @author DAVI MENEZES
 */
use Adianti\Widget\Base\TElement;

/**
 * Coluna bootstraps
 *
 * @version    Adianti 4.0
 * @package    grid bootstrap
 * @subpackage base
 * @author     Davi Menezes
 * @copyright  Copyright (c) 2017. (davimenezes.dvi@gmail.com)
 * @link https://bitbucket.org/DaviMenezes/dviadiandibootstrapgrid
 */
class DGridColumn extends TElement {

    private $child;

    public function __construct($child, $colClass, $colStyle)
    {
        parent::__construct('div');

        $this->class = $colClass;
        $this->style = 'min-height:36px; ' . $colStyle;

        $this->setChild($child);

        $this->add($this->getFormattedChild());
    }

    private function setChild($child)
    {
        $this->child = $child;
    }

    private function getFormattedChild()
    {
        $style = NULL;

        if( method_exists($this->child, 'getProperties') ) {
            $style = $this->child->getProperties()['style'] ?? NULL;
        } else if( method_exists($this->child, 'getProperty') ) {
            $style = $this->child->getProperty('style');
        }

        $currentStyle = 'height:36px;width:100%;' . $style;

        if( is_a($this->child, 'TLabel') ):
            $this->child->class .= ' control-label';
            //$this->child->style = $style .';width:100%; height: 20px; margin-bottom: 0; padding-top: 7px';
            $this->child->style = $currentStyle . '; margin-bottom: 0; padding-top: 7px';
        elseif( is_a($this->child, 'TSeekButton') ):
            $this->child->style .= ';min-width: calc(100% - 34px);';
            $this->child->setSize('calc(100% - 24px)', '36');
        elseif( is_a($this->child, 'TElement') ):
        //Todo - Analisar (com o ActionLink ficou bugado)
        //$this->child->style = $currentStyle .';height:auto';
        else:
            if( !is_a($this->child, 'TDataGrid')
                    AND ! is_a($this->child, 'TActionLink')
                    AND ! is_a($this->child, 'BootstrapDatagridWrapper')
                    AND ! is_a($this->child, 'THBox')
                    AND ! is_a($this->child, 'TElement')
            )
                $this->child->setSize('');
            //$this->child->class .= '; width100perc';
            $this->child->style = $currentStyle . '; width:100%;';
        endif;


        return $this->child;
    }

    /*
     * Especific use. Analysing...
     */

    public function addVField($child)
    {
        $this->setChild($child);
        $child = $this->getFormattedChild();
        $child->style .= '; margin-bottom:5px';
        $this->column->add($child);
    }

}