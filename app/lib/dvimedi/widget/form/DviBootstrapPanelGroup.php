<?php

namespace DviMedi\Widget\Form;

use Adianti\Widget\Form\TButton;
use Adianti\Widget\Container\TPanelGroup;
use DviMedi\Widget\Container\DHBox;
use DviMedi\Widget\Base\DGridBootstrap;
use Adianti\Control\TAction;
use Adianti\Widget\Form\TLabel;
use Adianti\Widget\Container\TNotebook;
use Adianti\Wrapper\BootstrapNotebookWrapper;
use Adianti\Widget\Form\TForm;
use Adianti\Widget\Util\TActionLink;

/**
 * Cria painéis com formulários personalisados
 *
 * @version    Adianti 4.0
 * @package    grid bootstrap
 * @subpackage base
 * @author     Davi Menezes
 * @copyright  Copyright (c) 2017. (davimenezes.dvi@gmail.com)
 * @link https://bitbucket.org/DaviMenezes/dviadiandibootstrapgrid
 */
class DviBootstrapPanelGroup extends TPanelGroup {

    private $className;
    private $gridRows;
    private $actions;
    private $actionButtons;
    private $hboxButtonsFooter;
    private $gridfields;
    private $hiddenFields = array();
    private $bootstrapClassDefault;
    private $form;
    private $formfields = array();
    private $tnotebook;
    private $tnotebookPage;
    private $tnotebookPages = array();

    /**
     * Ajuda a construir formulários com grid bootstrap customizáveis com rapidez
     * @param $className = The name current class
     * @param $title = The title of the panel Group
     * @param $columnTypeDefault = Bootstrap column class type (xs, sm, md, lg). Default is md
     * @param $methods = especific action methods [onSearch, onNew, onSave or onClear]
     */
    public function __construct(TForm $form, string $className, string $title = null, string $columnTypeDefault = 'md', array $methods = null)
    {
        parent::__construct($title);
        $this->class .= ' dvi';

        $this->className = $className;
        $this->bootstrapClassDefault = 'col-' . $columnTypeDefault . '-';
        $this->setForm($form);

        if( isset($methods['onSearch']) )
            $this->addActionSearch($methods['onSearch']);
        if( isset($methods['onNew']) )
            $this->addActionNew($methods['onNew']);
        if( isset($methods['onClear']) )
            $this->addActionClear($methods['onClear']);
        if( isset($methods['onSave']) )
            $this->addActionSave($methods['onSave']);
    }

    public function addFooterAction($callback, $image, $id = null, array $parameters = null, $tip = null, $label = null)
    {
        $this->addQuickAction($id ?? 'btn_' . uniqid(), $callback, $image, $parameters, $tip, $label);

        return $this;
    }

    //Todo verificar necessidade do parametro className
    public function addActionSearch($searchMethod = 'onSearch', array $parameters = null, $tip = null)
    {
        $this->addQuickAction('btnSearch', [$this->className, $searchMethod], 'fa:search fa-2x', $parameters, $tip);

        return $this;
    }

    //@param $callback Default is [$this->className,'onEdit']
    public function addActionNew(array $callback = NULL, array $parameters = null, $tip = null)
    {
        $action[] = $callback[0] ?? $this->className;
        $action[] = $callback[1] ?? 'onEdit';

        $this->addQuickAction('btnNew', $action, 'fa:plus fa-2x', $parameters, $tip);

        return $this;
    }

    public function addActionSave(string $saveMethod = 'onSave', array $parameters = null, $tip = null)
    {
        $this->addQuickAction('btnSave', [$this->className, $saveMethod], 'fa:floppy-o fa-2x', $parameters, $tip);

        return $this;
    }

    public function addActionClear(string $clearMethod = 'onClear', array $parameters = null, $tip = null)
    {
        $this->addQuickAction('btnClear', [$this->className, $clearMethod], 'fa:refresh fa-2x', $parameters, $tip);

        return $this;
    }

    public function addActionBackToList(string $className, string $method = 'onReload', $tip = null, array $parameters = null, $image = null, $label = null)
    {
        $this->addQuickAction('btnBack', [$className, $method], $image ?? 'fa:arrow-left fa-2x', $parameters, $tip, $label);

        return $this;
    }
    public function addActionLinkBackToList(array $callback, array $parameters, string $label= null, string $image = 'fa:arrow-left fa-2x')
    {
        $this->addActionButtonLink($label ? _t($label) : '', $callback, $parameters, $image );

        return $this;
    }

    //Add fields no necessary in Form
    public function addOffQuickFields()
    {
        $args = ['type' => 'off', 'fields' => func_get_args()];
        $this->addFieldForm($args);

        return $this;
    }

    /**
     * Add fields in form quickly.
     * Pass the parameters separated with commas
     * @example 1: "Field Name", $field1
     * @example 2: "Date", $dateStart, $dateEnd
     * @example 3: "Complex", [$field1, 'col-md-8 col-lg-10','font-color:red'], [$field2,'col-md-2']
     */
    public function addFields()
    {
        $params = (count(func_get_args()) == 1) ? func_get_arg(0) : func_get_args();

        $args = ['type' => 'inside', 'fields' => $params];
        $this->addFieldForm($args);

        return $this;
    }

    /**
     * Alias to addFields
     */
    public function addF()
    {
        $this->addFields(func_get_args());

        return $this;
    }

    public function addHiddenField($field)
    {
        $this->hiddenFields[] = $field;

        return $this;
    }

    public function addHiddenFields()
    {
        $fields = func_get_args();
        foreach ( $fields as $field ) {
            $this->hiddenFields[] = $field;
        }
        return $this;
    }

    public function addActionButtonLink(string $label, array $callback, array $parameters, string $image)
    {
        $this->actions[] = ['type' => 'link', 'callback' => $callback, 'image' => $image, 'parameters' => $parameters, 'label' => $label, 'class' => 'btn btn-default'];

        return $this;
    }

    public function addQuickAction(string $id, array $callback, string $image, array $parameters = null, $tip = null, $label = null)
    {
        $this->actions[] = ['type' => 'button', 'id' => $id, 'callback' => $callback, 'image' => $image, 'parameters' => $parameters, 'tip' => $tip, 'label' => $label];

        return $this;
    }

    private function setForm(&$form)
    {
        $form->class = 'form-horizontal';
        $this->form = $form;
    }

    public function createGrid()
    {
        $this->createGridFields();

        $this->addFieldsOnForm();

        if( $this->tnotebookPage ):
            $this->form->add($this->tnotebook);
        else:
            $this->form->add($this->gridfields);
        endif;

        $this->createActionsFooter();

        $this->add($this->form);

        if( $this->hboxButtonsFooter )
            $this->addFooter($this->hboxButtonsFooter);
    }

    private function createGridFields()
    {

        $this->gridfields = new DGridBootstrap;
        //$this->tnotebookPages; //Todo Check need

        $this->createRowsColumns();

        $this->createNotebookPages();
    }

    private function createNotebookPages()
    {
        if( $this->tnotebookPage ):
            $this->tnotebook = new BootstrapNotebookWrapper(new TNotebook);

            foreach ( $this->tnotebookPages as $pages ) {
                $this->tnotebook->appendPage($pages['title'], $pages['grid']);
            }
        endif;
    }

    private function createRowsColumns()
    {
        $tnotebookPage = NULL;
        //create Columns on the grid in format(col1 = label, col2 = any field, col3 = any field, etc)
        foreach ( $this->gridRows as $key => $columns ) {
            if( $columns['tnotebookPage'] != $tnotebookPage ):
                $tnotebookPage = $columns['tnotebookPage'];
                $this->gridfields = new DGridBootstrap;
                $this->tnotebookPages[] = ['title' => $columns['tnotebookPage'], 'grid' => $this->gridfields];
            endif;

            $row = $this->gridfields->addRow();

            $class = $this->getColumnClass($columns['cols']);

            //all other fields
            foreach ( $columns['cols'] as $key => $column ) {
                if( is_string($column['field']) ):
                    $classLabel = ( $key == 0 ) ? ' tiny_class' : NULL;
                    $row->addCol(new TLabel($column['field']), 'col-md-2' . $classLabel);
                else:
                    $row->addCol($column['field'], $column['class'] ?? $class, $column['style'] ?? '');
                endif;
            }
        }
    }

    private function getColumnClass($columns)
    {
        $qtdColumnsBootstrap = 12;
        $qtdColumnsToLabel = 2;
        $qtdColumnsString = 0;
        foreach ( $columns as $column ) {
            if( is_string($column['field']) )
                $qtdColumnsString ++;
        }
        $qtdValidFields = count($columns) - $qtdColumnsString;
        $restColumnDisponible = ($qtdColumnsBootstrap - ($qtdColumnsToLabel * $qtdColumnsString)) / ($qtdValidFields == 0 ? 1 : $qtdValidFields);
        $class = $this->bootstrapClassDefault . floor($restColumnDisponible);
        return $class;
    }

    //Get fields of the GridRows and add in form if it is a valid type
    private function addFieldsOnForm()
    {
        $this->getValidFields();

        $this->getHiddenFields();

        $this->getButtonsFields();

        foreach ( $this->formfields as $field ) {
            if( $this->validateField($field) )
                $this->form->addField($field);
        }
    }

    private function createActionsFooter()
    {
        if( $this->actionButtons ):
            $this->hboxButtonsFooter = new DHBox;
            foreach ( $this->actionButtons as $button ) {
                $this->hboxButtonsFooter->addButton($button);
            }
        endif;
    }

    /**
     * Insert and Define a page title
     * @param string $title Title of the tab
     */
    public function appendPage($title)
    {
        $this->tnotebookPage = $title;

        return $this;
    }

    //fields form - just get the field if it is of the valid type
    private function getValidFields()
    {
        foreach ( $this->gridRows as $row ) {
            foreach ( $row['cols'] as $column ) {
                if( is_a($column['field'], 'TDataGrid') )
                    $this->form->add($column['field']);
                elseif( $this->isImputInside($column) )
                    $this->formfields[] = $column['field'];
            }
        }
    }

    private function getHiddenFields()
    {
        foreach ( $this->hiddenFields as $field ) {
            $this->form->add($field); //important to get data via $param
            $this->form->addField($field); //important to get data via $form->getData()
        }
    }

    //ACTIONS BUTTONS
    private function getButtonsFields()
    {
        if( $this->actions ):
            foreach ( $this->actions as $value ) {
                $btn = $this->createButton($value);
                $this->actionButtons[] = $btn;
                $this->formfields[] = $btn;
            }
        endif;
    }

    private function addFieldForm(array $args)
    {
        $rowColumns = array();
        foreach ( $args['fields'] as $key => $arg ) {
            $element = array();
            if( is_array($arg) ) {
                //$element['label'] = $args[0];
                $element['field'] = $arg[0];
                $element['class'] = $arg[1] ?? NULL;
                $element['style'] = $arg[2] ?? NULL;
            } else {
                $element['field'] = $arg;
            }
            $element['type'] = $args['type'];

            $rowColumns[$key] = $element;
        }

        $this->gridRows[] = ['tnotebookPage' => $this->tnotebookPage, 'cols' => $rowColumns];
    }

    public function setCurrentNotebookPage(int $index)
    {
        $this->tnotebook->setCurrentPage($index);

        return $this;
    }

    public function setNotebookPageAction(array $callback, array $parameters = NULL)
    {
        $this->tnotebook->setTabAction(new TAction($callback, $parameters));

        return $this;
    }

    private function validateField($field)
    {
        $whiteList = ['THidden', 'TEntry', 'TButton', 'TCheckGroup', 'TColor', 'TCombo', 'TDate', 'TDateTime',
            'THidden', 'THtmlEditor', 'TMultiField', 'TFile', 'TMultiFile', 'TPassword', 'TRadioGroup',
            'TSeekButton', 'TDBSeekButton', 'TSelect', 'TSlider', 'TSpinner', 'TText'];

        $instanceClass = (string) \get_class($field);
        $array = explode('\\', $instanceClass);
        $classType = array_pop($array);

        if( in_array($classType, $whiteList) ):
            return TRUE;
        endif;

        return FALSE;
    }

    private function isImputInside(array $column)
    {
        if( $column['type'] == 'inside' AND ! is_string($column['field']) AND ! is_a($column['field'], 'TLabel') )
            return TRUE;

        return FALSE;
    }

    private function createButton($value)
    {
        if( $value['type'] == 'button' ):
            $btn = new TButton($value['id']);
            $btn->setAction(new TAction($value['callback'], $value['parameters']));
            $btn->setLabel($value['label']);
            $btn->setImage($value['image']);
            $btn->setTip($value['tip']);
            $btn->style = 'font-size: 14px;';
        elseif( $value['type'] == 'link' ):
            $action = new TAction($value['callback'], $value['parameters']);
            $btn = new TActionLink($value['label'], $action, '#333', '', '', $value['image']);
            $btn->class = $value['class'];
        endif;

        return $btn;
    }

}