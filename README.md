# Dvi Adianti Boostrap Grid


## Sobre
Essas classes surgiram da necessidade de criar e customizar as grids bootstrap com maior liberdade no Adianti Framework.  
Como o Adianti tem um foco muito forte em produtividade, então decidi seguir o mesmo caminho.  
Criar classes que possibilitassem a criação de grids de maneira rápida e intuitiva.  
Acredite isso pode ser um desafio e tanto.

É possível criar formulários mais customizáveis fazendo uso das grids bootstrap utilizando estas classes, porém mantendo a facilidade e rapidez nativa do Adianti.

A aparência nativa do template AdminLTE no Adianti Framework foi levemente alterada.

Caso não curta a aparência, nada impede que você altere ao seu gosto.

Estou fazendo o possível para deixar a responsabilidade da aparência nas folhas de estilo, então se ver algum código de estilo nos componentes não se assuste, este é um projeto em constante movimento e se quiser ajudar, ficarei feliz.

## Como Usar
$gridFields = new DGridBootstrap;  
$row1 = $gridFields->addRow();  
$row1->addLabel('Id');  
$row1->addCol($id, 'col-md-3');  
$lblDescription = new TLabel(_t('Description'));  
$row1->addCol($lblDescription, 'col-sm-12 hidden-md hidden-lg clear-fix');  
$row1->addCol($name, 'col-sm-12 col-md-7');  

$row2 = $gridFields->addRow();  
$row2->addLabel('Criado em');  
$row2->addCol($createdOn, 'col-sm-3', 'min-width:133px');  

$lblAte = new TLabel('até');  
$lblAte->style = 'text-align:left';  
$row2->addCol($lblAte, 'col-sm-1', 'padding-right:0;');  
$row2->addCol($createdOnEnd, 'col-sm-3', 'min-width:133px');  

### Explicando.

No Adianti temos algumas classes para criar formulários, mas a mais básica delas é a TForm  
portanto para criar um formulário usamos  
$form = new TForm('form_Name');  

Definimos uma classe css nativa para manter as aparências.  
$form->class = 'form-horizontal';  

### Criando os campos do formulário

Para criar um formulário customiado com grids bootstrap podemos usar a classe DGridBootstrap

#### DGridBootstrap
Responsável por representar uma grid bootstrap. Com ela é possível criar linhas e colunas adicionando os elementos através 
de métodos.

#### Entedendo uma grid
Uma grid é composta por linhas e colunas assim como uma tabela. Portanto precisamos de uma linha para adicionar colunas.

A forma mais comum de se adicionar uma coluna é através do método addCol da classe de "Linha".  
ex: linha->addCol();  
Este método aceita parâmetro sendo o primeiro um campo que acupará a coluna, o segundo são classes css, nela informamos 
a quantidade de colunas bootstrap que ela representa e quaisquer outras classes que julgar necessárias e o terceiro é uma 
string de estilos para customização visual. Veremos mais detalhes abaixo.

Nossa grid de campos  
$gridFields = new DGridBootstrap;

Cria uma linha  
$row1 = $gridFields->addRow();

adiciona um Label que por padrão tem tamanho de 2 colunas (col-md-2)  
$row1->addLabel('Id');

adiciona um coluna definindo apenas o tipo(md) e quantidade(3) de colunas bootstrap  
$row1->addCol($field_id, 'col-md-3');

criando e adicionando um Label com tamanho sm-12 passando classes css para formatação  
$lblDescription = new TLabel(_t('Description'));  
$row1->addCol($lblDescription, 'col-sm-12 hidden-md hidden-lg clear-fix');

customizando a coluna com mais de um tipo de coluna para diferentes dispositivos  
$row1->addCol($field_name, 'col-sm-12 col-md-7');

para adicionar mais linhas ...  
$row2 = $gridFields->addRow();

$row2->addLabel('Criado em');

$row2->addCol($createdOn, 'col-sm-3', 'min-width:133px');

$lblAte = new TLabel('até');  
$lblAte->style = 'text-align:left';  
$row2->addCol($lblAte, 'col-sm-1', 'padding-right:0;');  
$row2->addCol($createdOnEnd, 'col-sm-3', 'min-width:133px');

### Criando Formuários rápidos com grids bootstrap
Os formulários rápidos seguem a mesma base dos formulários tradicionais do Adianti porém com algumas modificações.  
Estes formulários foram estritos a partir da classe base TForm portanto tem aparência quase que totalmente nova.

Com esta classe é possível criar formulários com grids bootstrap fazendo uso da customização através das classes dos
bootstrap que você mesmo pode adicionar.

$formGrid = new DviBootstrapPanelGroup(self::$form, __CLASS__, 'Agendamento');  
$formGrid->addQuickFields('Campo1', $campo1);  
$formGrid->addQuickFields('Campo2', [$campo2, 'col-sm-4']);  

$formGrid->addActionClear();  
$formGrid->addActionSave();  
$formGrid->addActionButtonLink('Titulo do Botão', ['ClasseNome', 'onEdit'], ['campo_id'=>TSession::getValue(__CLASS__ . '_campo_id')], 'fa:plus fa-2x');  
$formGrid->addHiddenFields($id, $campo3, new THidden('campo4_id'));  
$formGrid->createGrid();

#### Explicando

Criando o painel que conterá o nosso formulário.  
O primeiro parâmetro espera uma instância de um TForm, o segundo espera o nome da classe atual e o terceiro espera o 
título do formulário  
$formGrid = new DviBootstrapPanelGroup(self::$form, __CLASS__, 'Agendamento');

Adicionando linhas e colunas ao formulário. Isto representa uma linha com 2 colunas com tamanho pré definido.  
$formGrid->addQuickFields('Campo1', $campo1);

estamos adicionando acões de limpar o formulário e de salvar. os métodos onClear e onSave devem existir na sua classe.  
$formGrid->addActionClear();  
$formGrid->addActionSave();

Adiciona uma acão no formato de Link  
$formGrid->addActionButtonLink('Campo label', ['Classe', 'onEdit'], ['id' => TSession::getValue(__CLASS__ . '_id')], 'fa:plus fa-2x');

Adiciona os campos invisíveis ao usuário, sem ocupar espaço visual na tela  
$formGrid->addHiddenFields($id, $campo3_id, new THidden('campo4_id'));

por fim preparamos e montamos tudo  
$formGrid->createGrid();

adiciona o formGrid na tela  
parent::add($formGrid);

Mais comumente usado com um container como mostrado abaixo.

$container = new TVBox;  
$container->style = 'width: 100%';  
$container->add($formGrid);  
$container->add($dataGrid); //se houver uma grid  

parent::add($container);

##Fluent Interface
### Existe outra forma ainda mais rápida de criar formulários que é fazendo uso do padrão Fluent Interface

$formGrid = new DviBootstrapPanelGroup($this->form, __CLASS__, 'Título do Formuário');  
$formGrid->addF('Campo1', $campo1)  
		->addF('Campo2', $campo2)  
		->addF('Campo3', $campo3)  
		->addF('', $campo4)  
		->addHiddenFields($id, $campo_hidden_2, $campo_hidden_3);

$formGrid->addActionClear();  
$formGrid->addActionSave();  
$formGrid->createGrid();

### Modo Completo
#### Customizando as colunas da grid bootstrap

$formGrid = new DviBootstrapPanelGroup($this->form, __CLASS__, 'Cronometria');  
$formGrid->addF('Campo1', $campo1)  
		->addF('Label1', [$campo1, 'col-md-4'], [new TLabel('fim'), 'col-md-1'], [$dateEnd, 'col-md-5'])  
		->addF('Medicamento', [$medicament_id, 'col-xs-2', 'padding-right:0px; min-width:170px'], [$btnNewMedicament, 'col-xs-1', 'padding:0 0 5px; width:46px'], [$medicament_name, 'col-sm-7'])  
		->addHiddenFields($id, $scheduling_prescription_id)  
		->addActionBackToList('PrescriptionForm', 'onEdit')  
		->addActionClear()  
		->addActionSave()  
		->createGrid();  
		
#### Explicando

Imagine que você tenha a necessidade de redimencionar suas colunas de acordo com o tamanho da tela do dispositivo.  
No Método addF pode receber vários parâmetros, que correspondem as colunas do seu grid.

Então se quiser criar uma linha com 2 colunas, passe 2 parâmetros.  
$formGrid->addF('Campo1', $campo1)  

Se quiser criar com 4 colunas e ainda informar as classes do bootstrap para diferenciar diferentes tamanhos de telas  
$formGrid->addF('Label1', [$campo1, 'col-md-4'], [$campo3, 'col-md-1'], [$campo4, 'col-md-5']) 

Se quiser formatar a coluna, informe os estilos como terceiro item do array de cada coluna que pretende formatar
$formGrid->addF('Label1', [$campo1, 'col-md-4', 'font-size:12px; color:red'], [$campo3, 'col-md-1'], [$campo4, 'col-md-5'])

Para mais informações entre em contato  
davimenezes.dvi@gmail.com